CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage
 * Maintainers


INTRODUCTION
------------

This module will help the developers to use the route options in Javascript.
If we are using ajax or any URL related functionalities we are either 
rendering through drupalSettings or we are using direct URL. Now if we change
the URL we need to update it in JS files to reflect the updated URL.

Instead if we can use this module it will provide 2 functions to get the alias
or the internalPath of the route provided. So we don't require any additional
settings to fetch the URL even if it is changed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/drupal_js_path

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/drupal_js_path



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See https://www.drupal.org/node/895232 for further information.
   
 * By Using composer, composer require "drupal/drupal_js_path"


USAGE
-----------
Below are the 2 functions which are available to utilize in JS.
Drupal.alias(routeName, routeParams, routeOptions);
Drupal.path(routeName, routeParams, routeOptions);


MAINTAINERS
-----------

Current maintainers:
 * Praveen Achanta (praveen3555) - https://www.drupal.org/u/praveen3555
