(function ($, Drupal, once, drupalSettings) {
  Drupal.behaviors.drupalJsPath = {
    attach: function (context, settings) {
      once('drupalJsPath', 'html', context).forEach(function (element) {
        // To return the alias from the provided route.
        Drupal.alias = function (route = '',routeParams = {}, routeOptions = {}) {
          var resultData = '';
          $.ajax({
            'url' : Drupal.url('find/alias/') + encodeURIComponent(route),
            'type' : 'post',
            "dataType": "json",
            "data": {params: routeParams, options: routeOptions},
            'async': false,
            'success': function (data) {
              if (data.message !== undefined) {
                console.log(data);
              }
              resultData = data;
            }
          });
          return resultData;
        };
        // To return the path from the provided route.
        Drupal.path = function (route = '',routeParams = {}, routeOptions = {}) {
          var resultData = '';
          $.ajax({
            'url' : Drupal.url('find/path/') + encodeURIComponent(route),
            'type' : 'post',
            "dataType": "json",
            "data": {params: routeParams, options: routeOptions},
            'async': false,
            'success': function (data) {
              if (data.message !== undefined) {
                console.log(data);
              }
              resultData = data;
            }
          });
          return resultData;
        };
      });
    }
  }
})(jQuery, Drupal, once, drupalSettings);