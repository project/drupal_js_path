<?php

namespace Drupal\drupal_js_path\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\Url;

/**
 * Drupal JS Path Class.
 */
class DrupalJsPath extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The route provider to load routes by name.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The request stack variable.
   *
   * @var \Drupal\Core\Http\RequestStack
   *   Provide the variable for request.
   */
  protected $requestStack;

  /**
   * The URL generator to use.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a \Drupal\drupal_js_path\Controller\DrupalJsPath object.
   *
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider to load routes by name.
   * @param \Drupal\Core\Http\RequestStack $request_stack
   *   Provide the variable for request.
   * @param \Symfony\Component\Routing\Generator\UrlGeneratorInterface $url_generator
   *   The URL generator.
   */
  public function __construct(RouteProviderInterface $route_provider, RequestStack $request_stack, UrlGeneratorInterface $url_generator) {
    $this->routeProvider = $route_provider;
    $this->requestStack = $request_stack;
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.route_provider'),
      $container->get('request_stack'),
      $container->get('url_generator')
    );
  }

  /**
   * Returns the path based out of route.
   */
  public function fetchAliasByRoute($route = "") {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->request->get('params') ?? [];
    $options = $request->request->get('options') ?? [];
    $url = '';
    try {
      if (!empty($this->routeProvider->getRoutesByNames([$route]))) {
        $url = $this->urlGenerator->generateFromRoute($route, $params, $options);
      }
    }
    catch (\Exception $e) {
      return new JsonResponse(['message' => $e->getMessage(), 'path' => NULL]);
    }
    return new JsonResponse(['path' => $url]);
  }

  /**
   * Returns the alias based out of route.
   */
  public function fetchPathByRoute($route = "") {
    $request = $this->requestStack->getCurrentRequest();
    $params = $request->request->get('params') ?? [];
    $options = $request->request->get('options') ?? [];
    $url = '';
    try {
      if (!empty($this->routeProvider->getRoutesByNames([$route]))) {
        $url = "/" . Url::fromRoute($route, $params, $options)->getInternalPath();
      }
    }
    catch (\Exception $e) {
      return new JsonResponse(['message' => $e->getMessage(), 'path' => NULL]);
    }
    return new JsonResponse(['path' => $url]);
  }

}
